<?php

namespace Drupal\suppress_alt_text\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;


/**
 * Configure example settings for this site.
 */
class SuppressAltTextForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'suppress_alt_text.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'suppress_alt_text_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $styles = ImageStyle::loadMultiple();

    $form['image_styles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Suppress alt text in these image styles:'),
      '#options' => array(),
      '#default_value' => $config->get('suppress_alt_text.image_styles'),
    );

    foreach ($styles as $key => $value) {
      $form['image_styles']['#options'][$key] = $key;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('suppress_alt_text.image_styles', $form_state->getValue('image_styles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
